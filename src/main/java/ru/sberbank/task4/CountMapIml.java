package ru.sberbank.task4;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class CountMapIml<T> implements CountMap<T> {

    private final List<T> key = new ArrayList<>();

    @Override
    public void add(T t) {
        this.key.add(t);
    }

    @Override
    public int getCount(T t) {
        int count = 0;
        for (T o : key) {
            if (o.equals(t)) count++;
        }
        return count;
    }

    @Override
    public int remove(T t) {
        int count = this.getCount(t);
        key.removeIf(e -> e.equals(t));

        return count;
    }

    @Override
    public int size() {
        Map<T, Integer> map = toMap();
        return map.size();
    }

    @Override
    public void addAll(CountMap<? extends T> source) {
        Map<? extends T, Integer> map = source.toMap();
        for (Map.Entry<? extends T, Integer> entry : map.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                this.add(entry.getKey());
            }
        }

    }

    @Override
    public Map<T, Integer> toMap() {
        Map<T, Integer> mapNewMap = key.stream().collect(Collectors.toMap(e -> e, e -> 1, Integer::sum));
        return mapNewMap;
    }

    @Override
    public void toMap(Map<T, Integer> destination) {
        Map<T, Integer> mapList = toMap();
        for (Map.Entry<T, Integer> entry : mapList.entrySet()) {
            int val = entry.getValue();
            if (destination.containsKey(entry.getKey())) {
                destination.computeIfPresent(entry.getKey(), (k, v) -> (v + val));
            } else destination.put(entry.getKey(), entry.getValue());

        }
    }


}
