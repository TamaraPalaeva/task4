package ru.sberbank.task4;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        CountMap<Integer> key = new CountMapIml<>();

        key.add(5);
        key.add(10);
        key.add(6);
        key.add(5);
        key.add(10);
        key.add(10);

//        System.out.println(key.getCount(5));
        System.out.println(key.remove(10));
        System.out.println(key.size());
//        key.toMap().forEach((k,v) -> System.out.println(k + ": " + v));

        CountMap<Integer> sorce = new CountMapIml<>();
        sorce.add(5);
        sorce.add(0);
        sorce.add(6);
        sorce.add(5);
        sorce.add(7);
        sorce.add(10);

        key.addAll(sorce);
        Map<Integer, Integer> mapSo = new HashMap<>();
        mapSo = sorce.toMap();
        key.toMap(mapSo);

        for (Map.Entry<Integer, Integer> entry : mapSo.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

       // key.toMap().forEach((k,v) -> System.out.println(k + ": " + v));


    }

}
