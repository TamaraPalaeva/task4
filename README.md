Посмотреть доклад 'Неочевидные Дженерики' c JeeConf. https://www.youtube.com/watch?v=H5WlE8BK5sI

Параметризовать CountMap и реализовать его (в реализации нельзя использовать Map). 

     public interface CountMap {
     
         // добавляет элемент в этот контейнер 
         void add(T t);
         
         //Возвращает количество добавлений данного элемента
         int getCount(T t);

         //Удаляет элемент и контейнера и возвращает количество его добавлений(до удаления)
         int remove(T t);

         //количество разных элементов
         int size();

          //Добавить все элементы из source в текущий контейнер, при совпадении ключей,     суммировать значения
         void addAll(CountMap source);

         //Вернуть java.util.Map. ключ - добавленный элемент, значение - количество его добавлений
         Map toMap();

         //Тот же самый контракт как и toMap(), только всю информацию записать в destination
         void toMap(Map destination);
    }

пример использования 
       
        CountMap<Integer> map = new CountMapIml<>();

        map.add(10);
        map.add(10);
        map.add(5);
        map.add(6);
        map.add(5);
        map.add(10);
        /*
                int count = map.getCout(5); // 2
                int count = map.getCout(6); // 1
                int count = map.getCout(10); // 3
        */

